const router = require('express').Router();
const {getLocalisationOfIp} = require('../../../utils/ip_localisation.js');
const {getAirPollutionFromCoords} = require('../../../utils/air_pollution');

router.all('/ping', async (req, res) => {
   const loc = await getLocalisationOfIp("89.86.35.89");
   const airPollution = await getAirPollutionFromCoords(loc.latitude, loc.longitude);
   res.json({pm25: airPollution.data.iaqi.pm25.v});
});

module.exports = router;
