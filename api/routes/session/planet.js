const router = require('express').Router();
const crypto = require('crypto');
const {Op} = require('sequelize');
const {SpaceError, getErrorMessage} = require('../../../utils/spaceError');
const {sendVerificationEmail} = require('../../../utils/sendVerificationEmail');
const {createPlanet, findOnePlanet, findAllPlanets,
    updatePlanet, countPlanet, findAllDTOSearchPlanets,
    findRandomDTOPlanets, findDTOPlanet} = require('../../models/helpers/planet_helper');
const {getRandomColor, rgbToHex} = require('../../../utils/colorUtils');

router.get('/planets', async (req, res) => {
    try{
        const value = req.query.value;
        if(!value)
            throw new SpaceError();

        const planets = await findAllPlanetsLike(value);

        return res.json({planets: planets});
    } catch (e) {
        return res.status(400).json({key: getErrorMessage('Error to find planets', e)});
    }
});

router.get('/planet/random', async (req, res) => {
    try{
        const {amount} = req.query;
        if(!amount || isNaN(amount))
            throw new SpaceError();

        const planets = await findRandomDTOPlanets(parseInt(amount));

        return res.status(200).json({planets: planets});
    } catch (e) {
        return res.status(400).json({key: getErrorMessage('Error to find planets', e)});
    }
});

router.get('/planet', async (req, res) => {
    try{
        const {id} = req.query;
        if(!id || isNaN(id))
            throw new SpaceError();

        const planet = await findDTOPlanet({id: id, verification_token: {[Op.eq]: null}});

        return res.json(planet);
    } catch (e) {
        return res.status(400).json({key: getErrorMessage('Error to find planet', e)});
    }
});

router.get('/planet/email', async (req, res) => {
    try{
        const {email} = req.query;
        if(!email)
            throw new SpaceError();

        const count = await countPlanet({email: email});

        if(count > 0)
            throw new SpaceError('Planet email already exists');

        return res.status(200).send();
    } catch (e) {
        return res.status(400).json({key: getErrorMessage('Error to find planet', e)});
    }
});


router.post('/planet', async (req, res) => {
    try{
        const {email, name} = req.body;
        if(!email || !name)
            throw new SpaceError();

        if(!await canCreatePlanet(email))
            throw new SpaceError('Email already use');

        const token = crypto.randomBytes(12).toString('hex');

        await createPlanet({email: email, name: name, verification_token: token});

        await sendVerificationEmail(email, token);

        return res.status(200).send();
    } catch (e) {
        return res.status(400).json({key: getErrorMessage('Error to create planet', e)});
    }
});

router.put('/claim', async (req, res) => {
    try{
        const {token} = req.body;
        if(!token)
            throw new SpaceError();

        const planet = await findOnePlanet({verification_token: token});

        await generateRandomPlanet(planet.dataValues.id);

        return res.status(200).send({id: planet.dataValues.id});
    } catch (e) {
        return res.status(400).json({key: getErrorMessage('Error in claiming your planet', e)});
    }
});

async function canCreatePlanet(email){
    return await countPlanet({email: email}) === 0;
}

async function findAllPlanetsLike(value){
    return await findAllDTOSearchPlanets({
        verification_token:{
            [Op.eq]: null
        },
        [Op.or]: [
            {
                email: {
                    [Op.like]: '%' + value + '%'
                }
            },
            {
                name: {
                    [Op.like]: '%' + value + '%'
                }
            },
        ]
    });
}

async function generateRandomPlanet(id){
    await updatePlanet(
        {
            verification_token: null,
            ocean_color: rgbToHex(0, randomInt(0, 255), 255),
            land_type: randomInt(1, 5),
            land_color: getRandomColor(),
            land_angle: randomInt(0, 359),
            clouds_type: randomInt(1, 3),
            clouds_angle: randomInt(0, 359),
            moon_land_type: randomInt(1, 4),
            moon_color: getRandomColor(),
            moon_angle: randomInt(0, 359),

            age: randomInt(1_000_000_000, 10_000_000_000),
            temperature: randomDouble(13, 17),
            mass: randomDouble(1, 10),
            mass_power: randomInt(24, 28),
            duration_of_year: randomDouble(200, 500),
            circumference: randomInt(15000, 100000)
        },
        {
            where: {id: id}
        }
    );
}

function randomInt(low, high) {
    return Math.floor(Math.random() * (high - low + 1) + low)
}

function randomDouble(min, max) {
    return Math.random() < 0.5 ? ((1-Math.random()) * (max-min) + min) : (Math.random() * (max-min) + min);
}

module.exports = router;
