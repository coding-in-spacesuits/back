const Planet = require('../planet_model');
const {Sequelize} = require('sequelize');
const {SpaceError} = require('../../../utils/spaceError');

async function createPlanet(values){
    try{
        const planet = Planet.create(values);
        return planet;
    } catch (e) {
        throw new SpaceError('Error when creating a planet');
    }
}

async function findOnePlanet(where){
    try {
        const planet = await Planet.findOne({where: where});
        if(planet)
            return planet;
    }catch (e) {
        throw new SpaceError('Error in finding a planet');
    }
    throw new SpaceError('Planet doesn\'t exist');
}

async function findAllPlanets(where) {
    try{
        const planets = await Planet.findAll({where: where});
        return planets;
    }catch (e) {
        throw new SpaceError('Error to find planets');
    }
}

async function updatePlanet(values, options, errorMessage = 'Planet update has failed'){
    try{
        const count = await Planet.update(values, options);
        return count;
    }catch (e) {
        throw new SpaceError(errorMessage);
    }
}

async function countPlanet(where){
    try {
        const amount = await Planet.count({where: where});
        return amount;
    }catch (e) {
        throw new SpaceError('Error to count planets');
    }
}

async function findAllDTOSearchPlanets(where){
    try {
        return await Planet.findAll({
            where: where,
            attributes: ['id', 'name']
        });
    }catch (e) {
        throw new SpaceError('Error to find planets');
    }
}

async function findRandomDTOPlanets(amount){
    try{
        return await Planet.findAll({
            where: {
                verification_token: null
            },
            order: [Sequelize.literal('RAND()')],
            limit: amount,
            attributes: ['id', 'name']
        });
    }catch (e) {
        console.log(e);
        throw new SpaceError('Error to find planets');
    }
}

async function findDTOPlanet(where){
    try {
        const planet = await Planet.findOne({
            where: where,
            attributes: {
                exclude: ['verification_token', 'email']
            }
        });
        if(planet)
            return planet;
    }catch (e) {
        throw new SpaceError('Error in finding a planet');
    }
    throw new SpaceError('Planet doesn\'t exist');
}

module.exports = {createPlanet, findOnePlanet, findAllPlanets, updatePlanet, countPlanet, findAllDTOSearchPlanets, findRandomDTOPlanets, findDTOPlanet};
