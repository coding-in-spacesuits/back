const {Sequelize} = require('sequelize');

const sequelize = new Sequelize(process.env.DB_DATABASE, process.env.DB_USER, process.env.DB_PASSWORD, {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: 'mysql',
    logging: false
});

function connection() {
    sequelize.authenticate().then(async () => {
        await sequelize.sync({force: false});
        console.error('Database connection etablished');
    }).catch(error => {
        console.error(`Error on database connection : ${error.message}`)
    });
}

module.exports = {sequelize: sequelize, connection: connection};
