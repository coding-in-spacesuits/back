const {DataTypes} = require('sequelize');
const {sequelize} = require('./database_connection');

const Planet = sequelize.define('planet', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        onDelete: 'CASCADE'
    },
    email: {
        type: DataTypes.STRING(320),
        allowNull: false,
        validate: {
            isEmail: true
        }
    },
    verification_token: {
        type: DataTypes.STRING(24),
        allowNull: true
    },
    name: {
        type: DataTypes.STRING(320),
        allowNull: false
    },

    ocean_color: {
        type: DataTypes.STRING(7),
        defaultValue: null
    },
    land_type: {
        type: DataTypes.INTEGER,
        defaultValue: 0
    },
    land_color: {
        type: DataTypes.STRING(7),
        defaultValue: null
    },
    land_angle: {
        type: DataTypes.INTEGER,
        defaultValue: 0
    },
    clouds_type: {
        type: DataTypes.INTEGER,
        defaultValue: 0
    },
    clouds_angle: {
        type: DataTypes.INTEGER,
        defaultValue: 0
    },
    moon_land_type: {
        type: DataTypes.INTEGER,
        defaultValue: 0
    },
    moon_color: {
        type: DataTypes.STRING(7),
        defaultValue: null
    },
    moon_angle: {
        type: DataTypes.INTEGER,
        defaultValue: 0
    },

    age: {
        type: DataTypes.BIGINT,
        defaultValue: 0
    },
    temperature: {
        type: DataTypes.DOUBLE,
        defaultValue: 0
    },
    mass: {
        type: DataTypes.DOUBLE,
        defaultValue: 0
    },
    mass_power: {
        type: DataTypes.INTEGER,
        defaultValue: 0
    },
    duration_of_year: {
        type: DataTypes.DOUBLE,
        defaultValue: 0.0
    },
    circumference: {
        type: DataTypes.INTEGER,
        defaultValue: 0
    }
}, {
    timestamps: false
});

module.exports = Planet;
