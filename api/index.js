const express = require('express');
const router = express.Router();

const ping = require('./routes/visitor/ping');

const planet = require('./routes/session/planet');

router.use('/visitor/', ping);

router.use('/session/', planet);

module.exports = router;
