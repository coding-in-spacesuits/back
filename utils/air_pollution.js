var axios = require('axios');

async function getAirPollutionFromCoords(lat, lng) {
    return axios
        .get(`https://api.waqi.info/feed/geo:${lat};${lng}/?token=${process.env.WAQI_TOKEN}`)
        .then((resp) => {
            return resp.data;
        })
        .catch(function(error) {
            console.log(error.message);
        });
}

module.exports = {getAirPollutionFromCoords: getAirPollutionFromCoords};
