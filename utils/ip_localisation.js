var axios = require('axios');

async function getLocalisationOfIp(ip) {
    return axios
        .get(`http://api.ipstack.com/${ip}?access_key=27e48ad1ccedc1626142ea5dc276ad6e`)
        .then((resp) => {
            return resp.data;
        })
        .catch(function(error) {
            console.log(error.message);
        });

}

module.exports = {getLocalisationOfIp: getLocalisationOfIp};