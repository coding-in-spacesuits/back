const nodemailer = require('nodemailer');

async function sendVerificationEmail(email, token){

    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: process.env.EMAIL_USER,
            pass: process.env.EMAIL_PASSWORD
        }
    });

    const validationLink = `${process.env.HOST}claim?token=${token}`;

    await transporter.sendMail({
        from: `"Spacesuits 🛰️" <${process.env.EMAIL_USER}>`, // sender address
        to: email, // list of receivers
        subject: "Claim email",
        html: '<br>Congratulations ! Your planet has freshly been created and life is ready to thrive on its surface 🧬</br>' +
            'Click on the link below to claim that you are the proud guardian of this sanctuary 🧑‍🚀</br>' +
            `<a href="${validationLink}">${validationLink}</a></p>`
    });
}

module.exports = {sendVerificationEmail: sendVerificationEmail};
