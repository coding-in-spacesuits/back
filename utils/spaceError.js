function SpaceError(message) {
    this.message = message;
    this.name = 'SpaceError';
}

function getErrorMessage(defaultMessage, e){
    return e instanceof SpaceError ? e.message : defaultMessage;
}

module.exports = {SpaceError: SpaceError, getErrorMessage};
