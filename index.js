const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');
const cors = require('cors');
const dotenv = require('dotenv');
const app = express();

app.use(cors());

dotenv.config();

const {connection} = require('./api/models/database_connection');

app.use(bodyParser.json({
    limit: '10mb'
}));

app.use(bodyParser.urlencoded({
    extended: false,
    limit: '10mb'
}));

const api = require('./api');
app.use("/api", api);

const port = process.env.PORT || 4000;
app.set('port', port);

const server = http.createServer(app);

connection();

server.listen(port, function() {
    console.log(`Server opened on port ${port}`);
});
